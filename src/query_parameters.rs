const AMOUNT: &str = "amount";
const SPL_TOKEN: &str = "spl-token";
const REFERENCE: &str = "reference";
const LABEL: &str = "label";
const MESSAGE: &str = "message";
const MEMO: &str = "memo";

#[derive(PartialEq, Clone, Hash, Eq)]
pub enum QueryParameter {
    Amount,
    SplToken,
    Reference,
    Label,
    Message,
    Memo,
    Extension(String),
}

impl ToString for QueryParameter {
    fn to_string(&self) -> String {
        self.as_str().to_string()
    }
}

impl QueryParameter {
    pub fn as_str(&self) -> &str {
        match self {
            Self::Amount => AMOUNT,
            Self::SplToken => SPL_TOKEN,
            Self::Reference => REFERENCE,
            Self::Label => LABEL,
            Self::Message => MESSAGE,
            Self::Memo => MEMO,
            Self::Extension(extension) => extension,
        }
    }
    pub fn allows_multiple(&self) -> bool {
        matches!(
            self,
            QueryParameter::Reference | QueryParameter::Extension(_)
        )
    }
}

impl From<&str> for QueryParameter {
    fn from(token: &str) -> QueryParameter {
        match token {
            AMOUNT => QueryParameter::Amount,
            SPL_TOKEN => QueryParameter::SplToken,
            REFERENCE => QueryParameter::Reference,
            LABEL => QueryParameter::Label,
            MESSAGE => QueryParameter::Message,
            MEMO => QueryParameter::Memo,
            _ => QueryParameter::Extension(token.to_string()),
        }
    }
}
