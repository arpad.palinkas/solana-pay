# solana-pay

Rust implementation of the [Solana Pay](https://docs.solanapay.com/) API.

Solana Pay is a standard protocol and set of reference implementations that enable developers to incorporate decentralized payments into their apps and services.

## About

This is a port of the [Official JavaScript SDK](https://github.com/solana-labs/solana-pay/tree/master/core) in pure Rust.

## License

Solana Pay is open source and available under the Apache License, Version 2.0. See the [LICENSE](./LICENSE) file for more info.
